from django.urls import path, include
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from api import views

router = routers.DefaultRouter()

router.register('users', views.UserViewSet)
router.register('groups', views.GroupViewSet)
# Привязываем наше API используя автоматическую маршрутизацию.
# Также мы подключим возможность авторизоваться в браузерной версии API.

urlpatterns = format_suffix_patterns([
    path("movie/", views.MovieViewSet.as_view({'get': 'list'}), name='movie_list'),
    path("movie/<int:pk>/", views.MovieViewSet.as_view({'get': 'retrieve'}), name='movie_detail'),
    path('actor/', views.ActorsViewSet.as_view({'get': 'list'}), name='actor_list'),
    path('actor/<int:pk>/', views.ActorsViewSet.as_view({'get': 'retrieve'}), name='actor_detail'),
])

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
