from django.test import TestCase
from movies.models import Actor, Genre, Movie, Category, MovieShots, Rating, Reviews, RatingStar
from django.core.files.uploadedfile import SimpleUploadedFile


class MoviesTestModel(TestCase):
    def test_movie(self):
        poster = SimpleUploadedFile("movie_image.jpg", content=b'', content_type='image/jpg')
        philip = Actor.objects.create(name="Philip")
        juliana = Actor.objects.create(name="K. Dick")
        movie = Movie.objects.create(
            title="Roger rabbit",
            tagline="Never back down",
            description="cool film",
            year='1990',
            country='America',
            world_premiere="2000-11-20",
            poster=poster,
            budget='5000',
            fess_in_usa='500',
            fess_in_world="2000",
            url="rabbit"
        )
        movie.actors.set([philip.pk, juliana.pk])
        movie.directors.set([philip.pk, juliana.pk])
        self.assertEqual(movie.url, "rabbit")

    def test_actor(self):
        actor = Actor.objects.create(name="statham", age="1")
        self.assertEquals(actor.age, '1')

    def test_category(self):
        category = Category.objects.create(name="Films", description="Фильмы")
        self.assertEqual(category.name, "Films")

    def test_genre(self):
        genre = Genre.objects.create(name="horror", description="horror")
        self.assertEqual(genre.description, "horror")

    def test_ratingstar(self):
        ratingstar = RatingStar.objects.create(value="5")
        self.assertEqual(ratingstar.value, "5")

    def test_movieshots(self):
        movie = Movie.objects.create(url="43")
        movieshots = MovieShots.objects.create(title="shots", description="1", movie=movie)
        self.assertEqual(movieshots.description, "1")

    def test_rating(self):
        movie = Movie.objects.create(url="43")
        ratingstar=RatingStar.objects.create(value=5)
        rating = Rating.objects.create(ip="125.15.1.1", star=ratingstar, movie=movie)
        self.assertEqual(rating.star.value, 5)

    def test_reviews(self):
        movie = Movie.objects.create(url="110")
        reviews = Reviews.objects.create(email="egor@mail.com", name="egor", text="cool film", movie=movie)
        self.assertEqual(reviews.movie.url, "110")
