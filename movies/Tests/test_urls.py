from django import test
from django.contrib.auth import get_user_model, authenticate
from django.test import TestCase, SimpleTestCase
from django.urls import reverse, resolve

from movies.views import FilterMoviesView, Search, AddStarRating, ActorView, MovieDetailView


class URLTests(test.TestCase):
    def test_homepage(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)


class SigninTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        self.user.save()

    def tearDown(self):
        self.user.delete()

    def test_correct(self):
        user = authenticate(username='test', password='12test12')
        self.assertTrue((user is not None) and user.is_authenticated)

    def test_wrong_username(self):
        user = authenticate(username='wrong', password='12test12')
        self.assertFalse(user is not None and user.is_authenticated)

    def test_wrong_password(self):
        user = authenticate(username='test', password='wrong')
        self.assertFalse(user is not None and user.is_authenticated)


class TestUrls(SimpleTestCase):
    def test_filter_url_is_resolved(self):
        url = reverse('filter')
        self.assertEquals(resolve(url).func.view_class, FilterMoviesView)

    def test_search_url_is_resolved(self):
        url = reverse('search')
        self.assertEquals(resolve(url).func.view_class, Search)

    def test_add_rating_url_is_resolved(self):
        url = reverse('add_rating')
        self.assertEquals(resolve(url).func.view_class, AddStarRating)

    def test_moviedetail_url_is_resolved(self):
        url = reverse('movie_detail', args=['some-slug'])
        self.assertEquals(resolve(url).func.view_class, MovieDetailView)

    def test_actorview_url_is_resolved(self):
        url = reverse('actor_detail', args=['some-slug'])
        self.assertEquals(resolve(url).func.view_class, ActorView)
