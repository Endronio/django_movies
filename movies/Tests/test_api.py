from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from api.serializers import MovieListSerializer, MovieDetailSerializer, ActorDetailSerializer
from movies.models import Movie, Actor


class BooksApiTestCase(APITestCase):
    def setUp(self):
        self.movie_1 = Movie.objects.create(title="terminator", year="1990", url='t')
        self.movie_2 = Movie.objects.create(title="robocop", year="1991", url='w')
        self.movie_3 = Movie.objects.create(title="rembo", year="1992", url='q')
        self.actor_1 = Actor.objects.create(name="Arnold", age="19")

    def test_get(self):
        url = reverse('movie_list')
        response = self.client.get(url)
        serializer_data = MovieListSerializer([self.movie_1, self.movie_2, self.movie_3], many=True).data
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(serializer_data, response.json().get("results"))

    def test_filter(self):
        url = reverse('movie_list')
        response = self.client.get(url, data={'year': 1990})
        serializer_data = MovieListSerializer([self.movie_1, self.movie_2, self.movie_3], many=True).data
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(serializer_data, response.json().get("results"))

    def test_ordering(self):
        url = reverse('movie_list')
        response = self.client.get(url, data={'ordering': 'year'})
        serializer_data = MovieListSerializer([self.movie_1, self.movie_2, self.movie_3], many=True).data
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(serializer_data, response.json().get("results"))

    def test_movie_list(self):
        response = self.client.get(reverse('movie_list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 3)

    def test_detail_movie(self):
        response = self.client.get(reverse('movie_detail', kwargs={"pk": self.movie_1.id}))
        serializer_data = MovieDetailSerializer(self.movie_1).data
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(serializer_data, response.data)

    def test_actors_list(self):
        response = self.client.get(reverse('actor_list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_detail_actor(self):
        response = self.client.get(reverse('actor_detail', kwargs={"pk": self.actor_1.id}))
        serializer_data = ActorDetailSerializer(self.actor_1).data
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(serializer_data, response.data)
