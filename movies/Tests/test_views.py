from django.contrib.auth import get_user_model
from django.test import TestCase, RequestFactory
from movies.models import Reviews, Movie
from movies.views import AddStarRating


class CreateNewReviewTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        self.movie = Movie.objects.create(
            url="5"
        )

        self.review = Reviews.objects.create(
            name='user1',
            email="test1@example.com",
            text='This is a test to check if a post is correctly created',
            movie=self.movie
        )


    def test_create_review_db(self):
        self.assertEqual(Reviews.objects.count(), 1)

    def test_rating_views(self):
        factory = RequestFactory()
        request = factory.get("add-rating/")
        request.user = self.user
        response = AddStarRating.as_view()(request)
        self.assertEqual(response.status_code, 405)


